import urllib.request
from driver.gmc_driver import GmcDriver
import os
from dotenv import load_dotenv

def check_vars():
    if 'GQ_USER_ID' not in os.environ:
        raise Exception("Missing GQ_USER_ID in the environmental variables")
    if 'GQ_GEIGER_ID' not in os.environ:
        raise Exception("Missing GQ_GEIGER_ID in the environmental variables")
    if 'GQ_DEVICE_PORT' not in os.environ:
        raise Exception("Missing the GQ device port in the environmental Variables")

def submit_data(user_id, geiger_id, cpm):
    url_string = "http://www.GMCmap.com/log2.asp?AID={}&GID={}&CPM={}"
    url = url_string.format(user_id, geiger_id, cpm)

    with urllib.request.urlopen(url) as f:
        if "OK.ERR0" not in f.read(300).decode():
            raise Exception("Error uploading data")

if __name__ == '__main__':
    load_dotenv('gq_vars.env')
    check_vars()
    gq = GmcDriver(os.environ['GQ_DEVICE_PORT'])
    cpm = gq.get_cpm()
    submit_data(os.environ['GQ_USER_ID'], os.environ['GQ_GEIGER_ID'], cpm)