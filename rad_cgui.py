"""
Console based GUI using curses for radiation meters from GQ
"""

import curses
from driver.gmc_driver import GmcDriver

class RadHistory:
    """
    History for the last 256 radiation measurements
    """

    def __init__(self):
        self._history = [0]*254

    def add_meas(self, meas):
        self._history.pop()
        self._history.append(meas)

    def get_history(self):
        return self._history



def get_device_info(gq):
    version = gq.get_version()
    device = version[:7]
    firmware = version[7:]
    sn = gq.get_serial_number()
    return [device, firmware, sn]

def get_summary_info(gq):
    cpm = gq.get_cpm()
    bat = gq.get_battery_voltage()
    return [cpm, bat]

def print_device_info(screen, startx, starty, gq):
    info = get_device_info(gq)
    screen.addstr(starty, startx, "Device: {:8}  Firmware: {:6}  Serial Number: {}".format(*info))

def print_summary(screen, startx, starty, gq):
    summary = get_summary_info(gq)
    screen.addstr(starty, startx, "CPM: {:4}   Voltage: {:4}".format(*summary))

def print_banner(screen, startx, starty):
    screen.addstr(starty, startx, "Radiation Counter GUI")

def main(screen):
    screen = curses.initscr()
    gq = GmcDriver()
    try:
        while True:
            print_banner(screen, 10, 0)
            print_device_info(screen, 0, 2, gq)
            print_summary(screen, 0, 3, gq)
            screen.refresh()
            curses.napms(1000)
    except:
        pass
    finally:
        curses.endwin()
        gq._close()

if __name__ == '__main__':
    curses.wrapper(main)