from .serial_base import SerialBase
import struct
import datetime

class GmcDriver(SerialBase):

    def _validate_response(self, response):
        if response == 0xAA:
            return True
        else:
            raise Exception("Command Error, Invalid Response")

    ####################### Measurements #######################

    def get_cpm(self):
        """
        Returns the current counts per minute (cpm) measured by
        the device.
        :return: CPM value
        """
        response = self._command_reponse("GETCPM", 2)
        [cpm] = struct.unpack('!H', response)
        return cpm

    def enable_heartbeat(self):
        """
        Enables the heartbeat, where the device sends the
        current counts per second (cps) measured.
        """
        self._send_command("HEARTBEAT1")

    def disable_heartbeat(self):
        """
        Disables the heartbeat, stopping the sending of
        the current cps
        """
        self._send_command("HEARTBEAT0")

    def get_heartbeat(self):
        """
        Attempts to get a cps measurement from the heartbeat.  First enable
        the heartbeat using enable_heartbeat
        :return: CPS value
        """
        resp = self._get_response(2)
        [raw_cps] = struct.unpack('!H', resp)
        # mask off the first 2 bits as these are reserved
        cps = (raw_cps & 0x3F)
        return cps

    def get_battery_voltage(self):
        """
        Returns the battery voltage in Volts
        :return: Battery Voltage in V
        """
        response = self._command_reponse("GETVOLT", 1)
        [raw_volt] = struct.unpack('!B', response)
        return raw_volt / 10

    def get_history(self, address, length):
        """
        Returns the history, generally not to exceed 4096 bytes.  Assumedly these
        are cpm data, the manual does not specify
        Note:
            GMC300Plus: Max Size is 64KBytes
        :param address: A positive number, with a maximum value depending on the
            device being used.
        :param length: Number of bytes to return
        :return: Byte array
        """
        address_b = struct.pack("!I", address)[1:]
        length_b = struct.pack("!H", length)
        commandstr = bytearray("SPIR".encode()) + address_b + length_b
        response_b = self._command_reponse(commandstr, length)
        return [dat for dat in struct.iter_unpack("!B", response_b)]

    ########################## System Calls ############################

    def power_on(self):
        self._send_command("POWERON")

    def power_off(self):
        self._send_command("POWEROFF")

    def reboot(self):
        self._send_command("REBOOT")

    ##################### Configuration Calls #######################

    def get_version(self):
        """
        Returns the version of the firmware on the device.
        :return: Firmware version
        """
        return self._command_reponse("GETVER", 14).decode()

    def get_serial_number(self):
        """
        Returns the serial number, a string in hexadecimal format to
        match how the number is reported on the geiger counters.
        :return: Serial Number string in Hex
        """
        resp = self._command_reponse("GETSERIAL", 7)
        longresp = struct.pack('!B', 0) + resp # Padding to 8 bytes for long long
        [serialnum] = struct.unpack("!Q", longresp)
        return "{:08X}".format(serialnum)

    def get_configuration(self):
        """
        Returns all the bytes of the configuration data
        :return: byte array of configuration data
        """
        resp = self._command_reponse("GETCFG", 256)
        return [dat for dat in struct.iter_unpack("!B", resp)]

    def erase_configuration(self):
        """
        Erases the configuration data.  Probably don't do this unless you know
        what you're doing.  Currently untested.
        """
        resp = self._command_reponse("ECFG", 1)
        self._validate_response(resp)

    def write_config_byte(self, address, data):
        """
        Writes a single byte to configuration data.  Probably don't use this
        unless you know what you're doing.  Currently untested.
        :param address: Address in memory from 0 to 255
        :param data: Single byte of data, formatted as int
        """
        data = struct.pack("!BB", address, data)
        commandstr = bytearray("WCFG".encode()) + data
        resp = self._command_response(commandstr, 1)
        self._validate_response(resp)

    def factory_reset(self):
        """
        Factory resets the device.  Untested.
        """
        resp = self._command_reponse("FACTORYRESET", 1)
        self._validate_response(resp)

    ########################### Date and Time #########################

    def get_date(self):
        """
        Returns the current date stored in the real time clock on the the device
        as a datetime object.
        :return: Current date on device
        """
        response = self._command_reponse("GETDATETIME", 7)
        print(response)
        self._validate_response(response[6])
        [year, month, day, hour, minute, second] = struct.unpack("!BBBBBB", response[:6])
        dt = datetime.datetime(2000+year, month, day, hour, minute, second)
        return dt

    def set_date(self, set_time=None):
        """
        Sets the unit to the provided set_time, default is the current date and time
        :param set_time: datetime object, or current time if none
        """
        if set_time is None:
            import datetime
            set_time = datetime.datetime.now()
        data = struct.pack("!BBBBBB", set_time.year-2000, set_time.month, set_time.day, set_time.hour, set_time.minute, set_time.second)
        commandstr = "SETDATETIME" + data.decode()
        resp = self._command_reponse(commandstr, 1)
        self._validate_response(resp[0])

    def set_year(self, year):
        """
        :param year: Year to set, must be post 1999
        """
        setyear = year-2000
        commandstr = "SETDATEYY" + struct.pack("!B", setyear).decode()
        resp = self._command_reponse(commandstr, 1)
        self._validate_response(resp[0])

    def set_month(self, month):
        """
        :param month:  Month to set
        """
        commandstr = "SETDATEMM" + struct.pack("!B", month).decode()
        resp = self._command_reponse(commandstr, 1)
        self._validate_response(resp[0])

    def set_day(self, day):
        """
        :param day: Day to set
        """
        commandstr = "SETDATEDD" + struct.pack("!B", day).decode()
        resp = self._command_reponse(commandstr, 1)
        self._validate_response(resp[0])

    def set_hour(self, hour):
        """
        :param hour: Hour of day to set
        """
        commandstr = "SETTIMEHH" + struct.pack("!B", hour).decode()
        resp = self._command_reponse(commandstr, 1)
        self._validate_response(resp[0])

    def set_minute(self, minute):
        commandstr = "SETTIMEMM" + struct.pack("!B", minute).decode()
        resp = self._command_reponse(commandstr, 1)
        self._validate_response(resp[0])

    def set_second(self, second):
        commandstr = "SETTIMESS" + struct.pack("!B", second).decode()
        resp = self._command_reponse(commandstr, 1)
        self._validate_response(resp[0])
