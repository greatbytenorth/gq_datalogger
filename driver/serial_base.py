"""
Serial base class

Can also be simulated
"""

SIM_SERIAL = False

from .virtual_serial import VirtSerial
import serial


class SerialBase:

    def __init__(self, serial_port=None, baud=115200):
        """
        Connects to a GMC Geiger Counter Serial Device
        :param serial_port:
        :param baud:
        """
        if serial_port is None:
            self.ser = VirtSerial()
        else:
            self.ser = serial.Serial(serial_port, baud, timeout = 1)

    def _close(self):
        self.ser.close()

    def _send_command(self, command):
        """
        Sends a command to the device, adding required formatting for sending
        commands.  If the command is a string, it will convert the command to
        a byte array before sending.
        :param command: The command to send
        """
        if isinstance(command, str):
            commandstr = '<' + str(command) + '>>'
            commandbin = commandstr.encode('utf-8')
        else:
            commandbin = '<'.encode() + command + '>>'.encode()
        self.ser.write(commandbin)

    def _get_response(self, byte_count = None):
        """
        Returns byte_count worth of bytes, or if byte_count is None, then it will
        return bytes until a newline is received
        :param byte_count: Number of bytes to return or return after newline
        :return: byte string
        """
        if byte_count is not None:
            recv = self.ser.read(byte_count)
        else:
            recv = self.ser.readline()
        return recv

    def _command_reponse(self, command, byte_count):
        """
        Clears buffers and then sends a command and returns byte_count worth
        of bytes in response.
        :param command:
        :param byte_count:
        :return:
        """
        self.ser.flush()
        self._send_command(command)
        return self._get_response(byte_count)


